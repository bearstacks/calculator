//
//  ViewController.swift
//  Calculator
//
//  Created by Muhammad Bilal Khawar on 2016-03-18.
//  Copyright © 2016 Muhammad Bilal Khawar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //IB Outlets
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var runningLabel: UILabel!
    @IBOutlet weak var zero: UIButton!
    @IBOutlet weak var one: UIButton!
    @IBOutlet weak var two: UIButton!
    @IBOutlet weak var three: UIButton!
    @IBOutlet weak var four: UIButton!
    @IBOutlet weak var five: UIButton!
    @IBOutlet weak var six: UIButton!
    @IBOutlet weak var seven: UIButton!
    @IBOutlet weak var eight: UIButton!
    @IBOutlet weak var nine: UIButton!
    @IBOutlet weak var dot: UIButton!
    @IBOutlet weak var equal: UIButton!
    @IBOutlet weak var plus: UIButton!
    @IBOutlet weak var minus: UIButton!
    @IBOutlet weak var multiply: UIButton!
    @IBOutlet weak var divide: UIButton!
    @IBOutlet weak var backspace: UIButton!
    @IBOutlet weak var answer: UIButton!
    @IBOutlet weak var allClear: UIButton!
    @IBOutlet weak var sine: UIButton!
    @IBOutlet weak var cosine: UIButton!
    @IBOutlet weak var tangent: UIButton!
    @IBOutlet weak var pi: UIButton!
    @IBOutlet weak var naturalLog: UIButton!
    @IBOutlet weak var logTen: UIButton!
    @IBOutlet weak var ePowerX: UIButton!
    @IBOutlet weak var eulersNumber: UIButton!
    @IBOutlet weak var squareRoot: UIButton!
    @IBOutlet weak var cubeRoot: UIButton!
    @IBOutlet weak var quarticRoot: UIButton!
    @IBOutlet weak var inverse: UIButton!
    @IBOutlet weak var xSquared: UIButton!
    @IBOutlet weak var xCubed: UIButton!
    @IBOutlet weak var xQuartic: UIButton!
    @IBOutlet weak var xPowerY: UIButton!
    @IBOutlet weak var openBracket: UIButton!
    @IBOutlet weak var closeBracket: UIButton!
    @IBOutlet weak var factorial: UIButton!
    @IBOutlet weak var tenPowerX: UIButton!
    
    //Initializers
    var runningCalc: String = " "
    var visibleCalc: String = " "
    let precedenceOfOperation = ["(": 0, "+": 1, "-": 1, "x": 2, "÷": 2, "s": 3, "c": 3, "t": 3, "l": 3, "n": 3, "^": 4, "!": 5, ")": 6]
    let operations: Set = ["+","-","x","÷","^","!","l","n","s","c","t","(",")"]
    let operations2: Set = ["x","÷","^","("]
    let operationStart: Set = ["s","c","t","n","l","("]
    var stackOperators = [String]()
    var stackNumbers = [String]()
    var runningCalcArray = [String]()
    var shuntingYardArray = [String]()
    var ans: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        hideAndShowButtons(self.view.frame.size)
        
        zero.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        one.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        two.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        three.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        four.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        five.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        six.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        seven.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        eight.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        nine.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        dot.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        equal.setBackgroundColor(UIColor(red: 0/255.0, green: 190.0/255.0, blue: 255.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        plus.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        minus.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        multiply.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        divide.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        answer.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        backspace.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        allClear.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        sine.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        cosine.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        tangent.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        pi.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        naturalLog.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        logTen.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        ePowerX.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        eulersNumber.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        squareRoot.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        cubeRoot.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        quarticRoot.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        inverse.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        xSquared.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        xCubed.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        xQuartic.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        xPowerY.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        openBracket.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        closeBracket.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        factorial.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        tenPowerX.setBackgroundColor(UIColor(red: 250.0/255.0, green: 225.0/255.0, blue: 50.0/255.0, alpha: 1.0), forUIControlState: .Highlighted)
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            totalLabel.font = UIFont.systemFontOfSize(96)
            runningLabel.font = UIFont.systemFontOfSize(56, weight: UIFontWeightLight)
        }
        
        for view in self.view.subviews {
            if let stackView = view as? UIStackView {
                for subView in stackView.subviews {
                    for subview2 in subView.subviews {
                        if let button = subview2 as? UIButton {
                            if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
                                button.layer.borderWidth = 0.5
                                button.layer.borderColor = UIColor.blackColor().CGColor
                                if button.currentTitle == "AC" {
                                    button.titleLabel?.font = UIFont.boldSystemFontOfSize(60)
                                } else if isNumberOrDot(button.currentTitle!) {
                                    button.titleLabel?.font = UIFont.systemFontOfSize(60)
                                } else {
                                    button.titleLabel?.font = UIFont.systemFontOfSize(60, weight: UIFontWeightThin)
                                }
                            } else {
                                button.layer.borderWidth = 0.25
                                button.layer.borderColor = UIColor.blackColor().CGColor
                            }
                        }
                        for subview3 in subview2.subviews {
                            if let button = subview3 as? UIButton {
                                if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
                                    button.layer.borderWidth = 0.5
                                    button.layer.borderColor = UIColor.blackColor().CGColor
                                    if button.currentTitle == "=" {
                                        button.titleLabel?.font = UIFont.boldSystemFontOfSize(60)
                                    } else if isNumberOrDot(button.currentTitle!) {
                                        button.titleLabel?.font = UIFont.systemFontOfSize(60)
                                    } else {
                                        button.titleLabel?.font = UIFont.systemFontOfSize(60, weight: UIFontWeightThin)
                                    }
                                } else {
                                    button.layer.borderWidth = 0.25
                                    button.layer.borderColor = UIColor.blackColor().CGColor
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        hideAndShowButtons(size)
    }
    
    //IBActions
    @IBAction func zeroPressed(sender: AnyObject) {
        numberPressed("0")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func onePressed(sender: AnyObject) {
        numberPressed("1")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func twoPressed(sender: AnyObject) {
        numberPressed("2")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func threePressed(sender: AnyObject) {
        numberPressed("3")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func fourPressed(sender: AnyObject) {
        numberPressed("4")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func fivePressed(sender: AnyObject) {
        numberPressed("5")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func sixPressed(sender: AnyObject) {
        numberPressed("6")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func sevenPressed(sender: AnyObject) {
        numberPressed("7")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func eightPressed(sender: AnyObject) {
        numberPressed("8")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func ninePressed(sender: AnyObject) {
        numberPressed("9")
        runningLabel.text = visibleCalc
    }
    
    @IBAction func dotPressed(sender: AnyObject) {
        runningCalc += "."
        visibleCalc += "."
        runningLabel.text = visibleCalc
    }
    
    @IBAction func plusPressed(sender: AnyObject) {
        runningCalc += "+"
        visibleCalc += "+"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func minusPressed(sender: AnyObject) {
        runningCalc += "-"
        visibleCalc += "-"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func multiplyPressed(sender: AnyObject) {
        runningCalc += "x"
        visibleCalc += "x"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func dividePressed(sender: AnyObject) {
        runningCalc += "÷"
        visibleCalc += "÷"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func answerPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xAns"
        } else {
            runningCalc += "Ans"
        }
        
        visibleCalc += "Ans"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func backspacePressed(sender: AnyObject) {
        
        if visibleCalc == " " {
            return
        }
        
        if String(visibleCalc.characters.suffix(4)) == "log(" || String(visibleCalc.characters.suffix(4)) == "sin(" || String(visibleCalc.characters.suffix(4)) == "cos(" || String(visibleCalc.characters.suffix(4)) == "tan(" {
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
        } else if String(runningCalc.characters.suffix(3)) == "ln(" || String(runningCalc.characters.suffix(3)) == "Ans" {
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
        } else if String(runningCalc.characters.suffix(2)) == "⁻¹" || String(runningCalc.characters.suffix(2)) == "³√" || String(runningCalc.characters.suffix(2)) == "⁴√" {
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
        } else {
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
            visibleCalc.removeAtIndex(visibleCalc.endIndex.predecessor())
        }
        
        while visibleCalc.characters.last != runningCalc.characters.last {
            runningCalc.removeAtIndex(runningCalc.endIndex.predecessor())
        }
        
        runningLabel.text = visibleCalc
    }
    
    @IBAction func allClearPressed(sender: AnyObject) {
        runningCalc = " "
        visibleCalc = " "
        totalLabel.text = "0"
        ans = "0"
        stackOperators = []
        stackNumbers = []
        runningCalcArray = []
        shuntingYardArray = []
        runningLabel.text = visibleCalc
    }
    
    @IBAction func sinePressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xsin("
        } else {
            runningCalc += "sin("
        }
        
        visibleCalc += "sin("
        runningLabel.text = visibleCalc
    }
    
    @IBAction func cosinePressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xcos("
        } else {
            runningCalc += "cos("
        }
        
        visibleCalc += "cos("
        runningLabel.text = visibleCalc
    }
    
    @IBAction func tangentPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xtan("
        } else {
            runningCalc += "tan("
        }
        
        visibleCalc += "tan("
        runningLabel.text = visibleCalc
    }
    
    @IBAction func piPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xπ"
        } else {
            runningCalc += "π"
        }
        
        visibleCalc += "π"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func naturalLogPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xln("
        } else {
            runningCalc += "ln("
        }
        
        visibleCalc += "ln("
        runningLabel.text = visibleCalc
    }
    
    @IBAction func logPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xlog("
        } else {
            runningCalc += "log("
        }
        
        visibleCalc += "log("
        runningLabel.text = visibleCalc
    }
    
    @IBAction func ePowerXPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xe^"
        } else {
            runningCalc += "e^"
        }
        
        visibleCalc += "e^"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func eulersNumberPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "xe"
        } else {
            runningCalc += "e"
        }
        
        visibleCalc += "e"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func squareRootPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "x√"
        } else {
            runningCalc += "√"
        }
        
        visibleCalc += "√"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func cubeRootPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "x³√"
        } else {
            runningCalc += "³√"
        }
        
        visibleCalc += "³√"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func quarticRootPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "x⁴√"
        } else {
            runningCalc += "⁴√"
        }
        
        visibleCalc += "⁴√"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func inversePressed(sender: AnyObject) {
        runningCalc += "⁻¹"
        visibleCalc += "⁻¹"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func xSquaredPressed(sender: AnyObject) {
        runningCalc += "²"
        visibleCalc += "²"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func xCubedPressed(sender: AnyObject) {
        runningCalc += "³"
        visibleCalc += "³"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func xQuarticPressed(sender: AnyObject) {
        runningCalc += "⁴"
        visibleCalc += "⁴"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func xPowerY(sender: AnyObject) {
        runningCalc += "^"
        visibleCalc += "^"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func openBracketPressed(sender: AnyObject) {
        if  xSignNeedsToBeAdded(runningCalc) {
            runningCalc += "x("
        } else {
            runningCalc += "("
        }
        
        visibleCalc += "("
        runningLabel.text = visibleCalc
    }
    
    @IBAction func closeBracketPressed(sender: AnyObject) {
        runningCalc += ")"
        visibleCalc += ")"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func factorialPressed(sender: AnyObject) {
        runningCalc += "!"
        visibleCalc += "!"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func tenPowerXPressed(sender: AnyObject) {
        runningCalc += "E"
        visibleCalc += "E"
        runningLabel.text = visibleCalc
    }
    
    @IBAction func equalPressed(Sender: AnyObject) {
        
        if visibleCalc == " " {
            return
        } else {
            runningCalc = runningCalc.stringByReplacingOccurrencesOfString(" ", withString: "")
        }
        
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("³√", withString: "∛")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("⁴√", withString: "∜")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("⁻¹", withString: "^(-1)")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("²", withString: "^(2)")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("³", withString: "^(3)")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("⁴", withString: "^(4)")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("E", withString: "x10^")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("Ans", withString: "\(ans)")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("sin", withString: "s")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("cos", withString: "c")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("tan", withString: "t")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("log", withString: "l")
        runningCalc = runningCalc.stringByReplacingOccurrencesOfString("ln", withString: "n")
        
        runningCalc = stringWithReplacedRoots(runningCalc, rootChar: "√", rootNum: "2")
        runningCalc = stringWithReplacedRoots(runningCalc, rootChar: "∛", rootNum: "3")
        runningCalc = stringWithReplacedRoots(runningCalc, rootChar: "∜", rootNum: "4")
        
        
        
        if runningCalc.containsString("+!") || runningCalc.containsString("-!") || runningCalc.containsString("x!") || runningCalc.containsString("÷!") || runningCalc.containsString("^!") || runningCalc.containsString("(!") {
            totalLabel.text = "SYNTAX ERROR"
            runningCalc = " "
            visibleCalc = " "
            ans = "0"
            stackOperators = []
            stackNumbers = []
            runningCalcArray = []
            shuntingYardArray = []
            return
        }
        
        var tempNum: String = ""
        var index: String.CharacterView.Index
        
        index = runningCalc.startIndex
        
        while index <= runningCalc.endIndex.predecessor() {
            while !operations.contains("\(runningCalc.characters[index])") {
                tempNum += "\(runningCalc.characters[index])"
                if index == runningCalc.endIndex.predecessor() {
                    break
                } else {
                    index = index.successor()
                }
            }
            if tempNum != ""{
                runningCalcArray.append("\(tempNum)")
                tempNum = ""
            }
            if operations.contains("\(runningCalc.characters[index])") {
                runningCalcArray.append("\(runningCalc.characters[index])")
            }
            if index == runningCalc.endIndex.predecessor() {
                break
            } else {
                index = index.successor()
            }
        }
        
        var indexOfArray: Int = 0
        while indexOfArray < runningCalcArray.count - 1 {
            if runningCalcArray[indexOfArray] == "-" && runningCalcArray[indexOfArray+1] == "-" {
                runningCalcArray.removeAtIndex(indexOfArray + 1)
                runningCalcArray[indexOfArray] = "+"
            } else if runningCalcArray[indexOfArray] == "-" && runningCalcArray[indexOfArray + 1] == "+" {
                runningCalcArray.removeAtIndex(indexOfArray + 1)
                runningCalcArray[indexOfArray] = "-"
            } else if runningCalcArray[indexOfArray] == "+" && runningCalcArray[indexOfArray + 1] == "-" {
                runningCalcArray.removeAtIndex(indexOfArray + 1)
                runningCalcArray[indexOfArray] = "-"
            } else if runningCalcArray[indexOfArray] == "+" && runningCalcArray[indexOfArray + 1] == "+" {
                runningCalcArray.removeAtIndex(indexOfArray + 1)
                runningCalcArray[indexOfArray] = "+"
            } else {
                indexOfArray = indexOfArray + 1
            }
        }
        
        //-#
        //+#
        if runningCalcArray.count > 1 {
            if runningCalcArray[0] == "-" && !operations.contains("\(runningCalcArray[1])") {
                runningCalcArray[0] = runningCalcArray[0] + runningCalcArray[1]
                runningCalcArray.removeAtIndex(1)
            } else if runningCalcArray[0] == "+" && !operations.contains("\(runningCalcArray[1])") {
                runningCalcArray.removeAtIndex(0)
            }
        }
        
        //- s,c,t,l,n
        //+ s,c,t'l,n
        if runningCalcArray.count > 1 {
            if runningCalcArray[0] == "-" && operationStart.contains("\(runningCalcArray[1])") {
                runningCalcArray[0] = "-1"
                runningCalcArray.insert("x", atIndex: 1)
            } else if runningCalcArray[0] == "+" && operationStart.contains("\(runningCalcArray[1])") {
                runningCalcArray.removeAtIndex(0)
            }
        }
        
        //x,/,^,(   -,+   #,s,c,t,l,n
        indexOfArray = 0
        while indexOfArray < runningCalcArray.count {
            if (indexOfArray == runningCalcArray.count - 2) || (runningCalcArray.count <= 2) {
                break
            } else {
                if operations2.contains("\(runningCalcArray[indexOfArray])") && runningCalcArray[indexOfArray + 1] == "-" && !operations.contains("\(runningCalcArray[indexOfArray + 2])") {
                    runningCalcArray[indexOfArray + 1] = runningCalcArray[indexOfArray + 1] + runningCalcArray[indexOfArray + 2]
                    runningCalcArray.removeAtIndex(indexOfArray + 2)
                } else if operations2.contains("\(runningCalcArray[indexOfArray])") && runningCalcArray[indexOfArray + 1] == "+" && !operations.contains("\(runningCalcArray[indexOfArray + 2])") {
                    runningCalcArray.removeAtIndex(indexOfArray + 1)
                } else if operations2.contains("\(runningCalcArray[indexOfArray])") && runningCalcArray[indexOfArray + 1] == "-" && operationStart.contains("\(runningCalcArray[indexOfArray + 2])") {
                    runningCalcArray[indexOfArray + 1] = "-1"
                    runningCalcArray.insert("x", atIndex: indexOfArray + 2)
                } else if operations2.contains("\(runningCalcArray[indexOfArray])") && runningCalcArray[indexOfArray + 1] == "+" && operationStart.contains("\(runningCalcArray[indexOfArray + 2])") {
                    runningCalcArray.removeAtIndex(indexOfArray + 1)
                } else {
                    indexOfArray = indexOfArray + 1
                }
            }
        }
        
        indexOfArray = 0
        while indexOfArray < runningCalcArray.count {
            if (indexOfArray == runningCalcArray.count - 1) || (runningCalcArray.count <= 1) {
                break
            } else {
                if operations2.contains("\(runningCalcArray[indexOfArray])") && runningCalcArray[indexOfArray + 1] == "-" && !operations.contains("\(runningCalcArray[indexOfArray + 2])") {
                    runningCalcArray[indexOfArray + 1] = runningCalcArray[indexOfArray + 1] + runningCalcArray[indexOfArray + 2]
                    runningCalcArray.removeAtIndex(indexOfArray + 2)
                } else if operations2.contains("\(runningCalcArray[indexOfArray])") && runningCalcArray[indexOfArray + 1] == "+" && !operations.contains("\(runningCalcArray[indexOfArray + 2])") {
                    runningCalcArray.removeAtIndex(indexOfArray + 1)
                } else if operations2.contains("\(runningCalcArray[indexOfArray])") && runningCalcArray[indexOfArray + 1] == "-" && operationStart.contains("\(runningCalcArray[indexOfArray + 2])") {
                    runningCalcArray[indexOfArray + 1] = "-1"
                    runningCalcArray.insert("x", atIndex: indexOfArray + 2)
                } else if operations2.contains("\(runningCalcArray[indexOfArray])") && runningCalcArray[indexOfArray + 1] == "+" && operationStart.contains("\(runningCalcArray[indexOfArray + 2])") {
                    runningCalcArray.removeAtIndex(indexOfArray + 1)
                } else {
                    indexOfArray = indexOfArray + 1
                }
            }
        }
        
        
        for item in runningCalcArray {
            if !operations.contains("\(item)") {
                shuntingYardArray.append(item)
            } else if operations.contains("\(item)") {
                if item == "(" {
                    stackOperators.append(item)
                } else if item == ")" {
                    if stackOperators.isEmpty {
                        shuntingYardArray.append(item)
                    } else {
                        while stackOperators.last != "(" && !stackOperators.isEmpty {
                            shuntingYardArray.append(stackOperators.last!)
                            stackOperators.removeLast()
                            if stackOperators.last != "(" && stackOperators.count == 1 {
                                shuntingYardArray.append(stackOperators.last!)
                                shuntingYardArray.append(item)
                                stackOperators.removeLast()
                            }
                        }
                        if !stackOperators.isEmpty {
                            stackOperators.removeLast()
                        }
                    }
                } else if operations.contains("\(item)") && stackOperators.isEmpty {
                    stackOperators.append(item)
                } else if operations.contains("\(item)") && !stackOperators.isEmpty {
                    if precedenceOfOperation[item] > precedenceOfOperation[stackOperators.last!] {
                        stackOperators.append(item)
                    } else {
                        while (precedenceOfOperation[item] <= precedenceOfOperation[stackOperators.last!]) {
                            shuntingYardArray.append(stackOperators.last!)
                            stackOperators.removeLast()
                            if stackOperators.isEmpty {
                                break
                            }
                        }
                        stackOperators.append(item)
                    }
                }
            }
        }
        
        while !stackOperators.isEmpty {
            shuntingYardArray.append(stackOperators.last!)
            stackOperators.removeLast()
        }
        
        if shuntingYardArray.contains("(") || shuntingYardArray.contains(")") {
            totalLabel.text = "SYNTAX ERROR"
            runningCalc = " "
            visibleCalc = " "
            ans = "0"
            stackOperators = []
            stackNumbers = []
            runningCalcArray = []
            shuntingYardArray = []
            return
        } else {
            for item in shuntingYardArray {
                if !operations.contains("\(item)") {
                    if let _ = Double(item) {
                        stackNumbers.append("\(item)")
                    } else if item == "π" || item == "-π" || item == "e" || item == "-e" {
                        stackNumbers.append("\(item)")
                    } else {
                        totalLabel.text = "SYNTAX ERROR"
                        runningCalc = " "
                        visibleCalc = " "
                        ans = "0"
                        stackOperators = []
                        stackNumbers = []
                        runningCalcArray = []
                        shuntingYardArray = []
                        return
                    }
                } else {
                    if item == "+" || item == "-" || item == "x" || item == "÷" || item == "^" {
                        if stackNumbers.count >= 2 {
                            let total = doCalclationWithTwoOperands(item, operand1: stackNumbers[stackNumbers.count - 2], operand2: stackNumbers[stackNumbers.count - 1])
                            stackNumbers.removeLast()
                            stackNumbers[stackNumbers.count - 1] = total
                        } else {
                            totalLabel.text = "SYNTAX ERROR"
                            runningCalc = " "
                            visibleCalc = " "
                            ans = "0"
                            stackOperators = []
                            stackNumbers = []
                            runningCalcArray = []
                            shuntingYardArray = []
                            return
                        }
                    } else if item == "n" || item == "l" || item == "s" || item == "c" || item == "t" {
                        if stackNumbers.count >= 1 {
                            let total = doCalclationWithOneOperands(item, operand: stackNumbers[stackNumbers.count - 1])
                            stackNumbers[stackNumbers.count - 1] = total
                        } else {
                            totalLabel.text = "SYNTAX ERROR"
                            runningCalc = " "
                            visibleCalc = " "
                            ans = "0"
                            stackOperators = []
                            stackNumbers = []
                            runningCalcArray = []
                            shuntingYardArray = []
                            return
                        }
                    } else if item == "!" {
                        if stackNumbers.count >= 1 {
                            if let _ = Int(stackNumbers.last!) {
                                let total = doCalclationWithFactorial(stackNumbers[stackNumbers.count - 1])
                                stackNumbers[stackNumbers.count - 1] = total
                            } else {
                                totalLabel.text = "SYNTAX ERROR"
                                runningCalc = " "
                                visibleCalc = " "
                                ans = "0"
                                stackOperators = []
                                stackNumbers = []
                                runningCalcArray = []
                                shuntingYardArray = []
                                return
                            }
                        } else {
                            totalLabel.text = "SYNTAX ERROR"
                            runningCalc = " "
                            visibleCalc = " "
                            ans = "0"
                            stackOperators = []
                            stackNumbers = []
                            runningCalcArray = []
                            shuntingYardArray = []
                            return
                        }

                    }
                }
            }
        }
        
        if stackNumbers.count != 1 {
            totalLabel.text = "SYNTAX ERROR"
            runningCalc = " "
            visibleCalc = " "
            ans = "0"
            stackOperators = []
            stackNumbers = []
            runningCalcArray = []
            shuntingYardArray = []
            return
        } else {
            if stackNumbers[0] == "π" {
                stackNumbers[0] = "\(M_PI)"
            } else if stackNumbers[0] == "π" {
                stackNumbers[0] = "\(-1 * M_PI)"
            } else if stackNumbers[0] == "e" {
                stackNumbers[0] = "\(M_E)"
            } else if stackNumbers[0] == "-e" {
                stackNumbers[0] = "\(-1 * M_E)"
            }

            if String(stackNumbers[0].characters.suffix(2)) == ".0" {
                stackNumbers[0] = stackNumbers[0][stackNumbers[0].characters.startIndex...stackNumbers[0].characters.endIndex.predecessor().predecessor().predecessor()]
            }
            
            totalLabel.text = stackNumbers[0]
            
            ans = stackNumbers[0]
            runningCalc = " "
            visibleCalc = " "
            stackOperators = []
            stackNumbers = []
            runningCalcArray = []
            shuntingYardArray = []
        }
    }
    
    func doCalclationWithTwoOperands(currentOperator: String, operand1: String, operand2: String) -> String {
        var number1: Double
        var number2: Double
        var total: Double = 0
        
        if isPiPositive(operand1) {
            number1 = M_PI
        } else if isPiNegative(operand1) {
            number1 = -1 * M_PI
        } else if isEulerPositive(operand1) {
            number1 = M_E
        } else if isPiNegative(operand1) {
            number1 = -1 * M_E
        } else {
            number1 = Double(operand1)!
        }
        
        if isPiPositive(operand2) {
            number2 = M_PI
        } else if isPiNegative(operand2) {
            number2 = -1 * M_PI
        } else if isEulerPositive(operand2) {
            number2 = M_E
        } else if isPiNegative(operand2) {
            number2 = -1 * M_E
        } else {
            number2 = Double(operand2)!
        }
        
        if currentOperator == "+" {
            total = number1 + number2
        } else if currentOperator == "-" {
            total = number1 - number2
        } else if currentOperator == "x" {
            total = number1 * number2
        } else if currentOperator == "÷" {
            total = number1 / number2
        } else if currentOperator == "^" {
            total = pow(number1, number2)
        }
        
        return String(total)
    }
    
    func doCalclationWithOneOperands(currentOperator: String, operand: String) -> String {
        var number: Double
        var total: Double = 0
        
        if isPiPositive(operand) {
            number = M_PI
        } else if isPiNegative(operand) {
            number = -1 * M_PI
        } else if isEulerPositive(operand) {
            number = M_E
        } else if isPiNegative(operand) {
            number = -1 * M_E
        } else {
            number = Double(operand)!
        }
        
        if currentOperator == "l" {
            total = log10(number)
        } else if currentOperator == "n" {
            total = log(number)
        } else if currentOperator == "s" {
            total = sin(number)
        } else if currentOperator == "c" {
            total = cos(number)
        } else if currentOperator == "t" {
            total = tan(number)
        }
        
        return String(total)
    }
    
    func doCalclationWithFactorial(operand: String) -> String {
        var number: Int
        var total: Int = 0
        
        number = Int(operand)!
        
        total = factorials(number)
        
        return String(total)
    }
    
    func factorials(number: Int) -> (Int) {
        if number <= 1 {
            return 1
        }
        
        return number * factorials(number - 1)
    }

    func isNumber(char : Character) -> Bool {
        return char == "0" || char == "1" || char == "2" || char == "3" || char == "4" || char == "5" || char == "6" || char == "7" || char == "8" || char == "9"
    }
    
    func isNumberOrDot(str: String) -> Bool {
        return str == "0" || str == "1" || str == "2" || str == "3" || str == "4" || str == "5" || str == "6" || str == "7" || str == "8" || str == "9" || str == "."
    }
    
    func isPiOrEuler(char : Character) -> Bool {
        return char == "π" || char == "e"
    }
    
    func isPiPositive(string: String) -> Bool {
        if string == "π" {
            return true
        } else {
            return false
        }
    }
    
    func isPiNegative(string: String) -> Bool {
        if string == "-π" {
            return true
        } else {
            return false
        }
    }
    
    func isEulerPositive(string: String) -> Bool {
        if string == "e" {
            return true
        } else {
            return false
        }
    }
    
    func isEulerNegative(string: String) -> Bool {
        if string == "-e" {
            return true
        } else {
            return false
        }
    }
    
    func numberPressed(char: Character) {
        if (isPiOrEuler(runningCalc.characters.last!)) || (runningCalc.characters.last == ")") || (runningCalc.characters.last == "!") || (runningCalc.characters.last == "¹") || (runningCalc.characters.last == "²") || (runningCalc.characters.last == "³") || (runningCalc.characters.last == "⁴") || (runningCalc.characters.last == "s") {
            runningCalc += "x\(char)"
        } else {
            runningCalc += "\(char)"
        }
        
        visibleCalc += "\(char)"
    }
    
    func xSignNeedsToBeAdded(string: String) -> Bool {
        if (isNumber(string.characters.last!)) || (isPiOrEuler(string.characters.last!)) || (string.characters.last == ")") || (string.characters.last == "¹") || (string.characters.last == "²") || (string.characters.last == "³") || (string.characters.last == "⁴") || (string.characters.last == "!") || (string.characters.last == ".") || (string.characters.last == "s") {
            return true
        } else {
            return false
        }
    }
    
    func stringWithReplacedRoots(str: String, rootChar: Character, rootNum: Character) -> String {
        
        var index: String.CharacterView.Index
        
        var string = str
        
        if string.characters.count == 1 && string.containsString("\(rootChar)") {
            string.removeAll()
            string += "^(1÷\(rootNum))"
        } else {
            while string.containsString("\(rootChar)") {
                index = string.characters.indexOf(rootChar)!
                if index == string.endIndex.predecessor() && index != string.startIndex {
                    string.removeAtIndex(index)
                    index = index.predecessor()
                }else {
                    string.removeAtIndex(index)
                }
                while string.characters[index] != "+" && string.characters[index] != "-" && string.characters[index] != "x" && string.characters[index] != "÷" && string.characters[index] != "^" && index != string.endIndex.predecessor() {
                    index = index.successor()
                }
                if index == string.endIndex.predecessor() {
                    string += "^(1÷\(rootNum))"
                } else {
                    string.insert(")", atIndex: index)
                    string.insert(rootNum, atIndex: index)
                    string.insert("÷", atIndex: index)
                    string.insert("1", atIndex: index)
                    string.insert("(", atIndex: index)
                    string.insert("^", atIndex: index)
                }
            }
        }
        return string
    }
    
    func hideAndShowButtons(size: CGSize) {
        
        if size.width > size.height {
            sine.hidden = false
            cosine.hidden = false
            tangent.hidden = false
            pi.hidden = false
            naturalLog.hidden = false
            logTen.hidden = false
            ePowerX.hidden = false
            eulersNumber.hidden = false
            squareRoot.hidden = false
            cubeRoot.hidden = false
            quarticRoot.hidden = false
            inverse.hidden = false
            xSquared.hidden = false
            xCubed.hidden = false
            xQuartic.hidden = false
            xPowerY.hidden = false
            openBracket.hidden = false
            closeBracket.hidden = false
            factorial.hidden = false
            tenPowerX.hidden = false
        }
        
        if size.height > size.width {
            sine.hidden = true
            cosine.hidden = true
            tangent.hidden = true
            pi.hidden = true
            naturalLog.hidden = true
            logTen.hidden = true
            ePowerX.hidden = true
            eulersNumber.hidden = true
            squareRoot.hidden = true
            cubeRoot.hidden = true
            quarticRoot.hidden = true
            inverse.hidden = true
            xSquared.hidden = true
            xCubed.hidden = true
            xQuartic.hidden = true
            xPowerY.hidden = true
            openBracket.hidden = true
            closeBracket.hidden = true
            factorial.hidden = true
            tenPowerX.hidden = true
        }
    }
}

extension UIButton {
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func setBackgroundColor(color: UIColor, forUIControlState state: UIControlState) {
        self.setBackgroundImage(imageWithColor(color), forState: state)
    }
}

